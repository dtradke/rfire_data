# RFire
# 11/16/19
#
# New way to look at "bins" from the mmWave board.
# To run if one file: python3 viz_range_data.py [file]
# To run if a folder full of files: python3 viz_range_data.py [folder]
# import importlib
# importlib.import_module('mpl_toolkits').__path__
# from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt
# import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


from scipy.optimize import least_squares
# from sklearn.cross_validation import train_test_split
# Import function to automatically create polynomial features!
from sklearn.preprocessing import PolynomialFeatures
# Import Linear Regression and a regularized regression function
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LassoCV
# Finally, import function to make a machine learning pipeline
from sklearn.pipeline import make_pipeline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from sklearn.decomposition import PCA

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir

def threeDimPlot(df):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    cols = df.columns
    time = len(df[cols[0]].tolist())
    bins = len(cols)
    Y = []
    for x in range(0, time):
        Y.append(np.arange(0,bins,1))
    Y = np.array(Y)

    X = []
    for x in range(0, bins):
        X.append(np.arange(0,time,1))
    X = np.array(X)
    Z = df.values
    Z = Z[:,:bins] #only takes the first n distances

    # Make data.
    X = np.arange(0, time, 1)
    Y = np.arange(0, bins, 1)
    Y, X = np.meshgrid(X, Y)
    Z = np.transpose(Z)

    # print(X.shape)
    # print(Y.shape)
    # print(Z.shape)
    # exit()

    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

    # Customize the z axis.
    # ax.set_zlim(0, 50000000000)
    # ax.zaxis.set_major_locator(LinearLocator(10))
    # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title(sys.argv[1], fontsize=18)
    plt.ylabel('Timesteps', fontsize=15)
    plt.xlabel('Distance Bin', fontsize=15)
    plt.show()


def get_scatter(df):

    dist_start = 4
    dist_stop = 50
    min_fire_bin = 23

    all_avg = 0
    all_max = 0
    all_min = 10
    all_count = 0

    data = {'fire':{'x':[], 'y':[], 'z':[]},
            'nofire':{'x':[], 'y':[], 'z':[]}}

    for j in range(dist_start, dist_stop):
        all_count += 1
        fire_flag = False

        target_distance = j
        # 45, 89, 135, 181 fires

        start_time = 0
        end_time = 300
        # start_time = 750
        # end_time = 1050
        step = 10

        fire_start = 600

        total_max = 0
        total_min = 10
        total_total = 0
        total_count = 0

        for i, col in enumerate(df.columns):
            if i == target_distance:
                print("Distance: ", target_distance)
                col_list = df[col].tolist()
                while end_time < len(col_list):
                    sample_max = 0
                    sample_min = 100
                    if end_time > fire_start and start_time < fire_start and j >= min_fire_bin:
                        fire_flag = True

                    total_count += 1
                    # col_list = np.log10(col_list[800:1312])
                    col_list1 = np.array(col_list[start_time:end_time])
                    col_list1 = np.absolute(np.fft.fft(col_list1))
                    col_list1 = np.divide(col_list1[1:], col_list1[0])

                    max_idx = argrelextrema(col_list1, np.greater)
                    min_idx = argrelextrema(col_list1, np.less)
                    maxs = col_list1[argrelextrema(col_list1, np.greater)[0]]
                    mins = col_list1[argrelextrema(col_list1, np.less)[0]]

                    try:
                        if max_idx[0][0] < min_idx[0][0]:
                            a = maxs
                            b = mins
                        else:
                            a = mins
                            b = maxs

                        c = np.empty((a.size + b.size,), dtype=a.dtype)
                        c[0::2] = a
                        c[1::2] = b

                    except:
                        if max_idx[0].size == 0:
                            c = mins
                        elif min_idx[0].size == 0:
                            c = maxs
                        else:
                            c = np.array([])


                    total_diff = 0
                    count = 0
                    for key, val in enumerate(c):
                        try:
                            diff = abs(val - c[key+1])
                            total_diff+=diff
                            if diff > sample_max:
                                sample_max = diff
                            if diff < sample_min:
                                sample_min = diff
                            count+=1
                        except:
                            pass

                    try:
                        avg = total_diff/count
                    except:
                        avg = 0


                    if fire_flag:
                        data['fire']['x'].append(sample_max)
                        data['fire']['y'].append(sample_min)
                        data['fire']['z'].append(avg)
                    else:
                        data['nofire']['x'].append(sample_max)
                        data['nofire']['y'].append(sample_min)
                        data['nofire']['z'].append(avg)

                    start_time+=step
                    end_time+=step



    return data


def makePCA(df, dist_start):

    dist_stop = dist_start + 1
    min_fire_bin = 23

    all_avg = 0
    all_max = 0
    all_min = 10
    all_count = 0

    data = {'fire':{'x':[], 'y':[], 'z':[]},
            'nofire':{'x':[], 'y':[], 'z':[]}}

    pca = PCA(n_components=3)
    x = []

    for j in range(dist_start, dist_stop):
        all_count += 1
        fire_flag = False

        target_distance = j
        # 45, 89, 135, 181 fires

        start_time = 0
        end_time = 300
        # start_time = 750
        # end_time = 1050
        step = 10

        fire_start = 600

        total_max = 0
        total_min = 10
        total_total = 0
        total_count = 0


        for i, col in enumerate(df.columns):
            if i == target_distance:
                print("Distance: ", target_distance)
                col_list = df[col].tolist()
                while end_time < len(col_list):
                    sample_max = 0
                    sample_min = 100
                    if end_time > fire_start and start_time < fire_start and j >= min_fire_bin and not fire_flag:
                        fire_flag = True
                        pc = pca.fit_transform(np.array(x))
                        data['nofire']['x'].append(pc[:,0].tolist())
                        data['nofire']['y'].append(pc[:,1].tolist())
                        data['nofire']['z'].append(pc[:,2].tolist())
                        x = []

                    total_count += 1
                    # col_list = np.log10(col_list[800:1312])
                    col_list1 = np.array(col_list[start_time:end_time])
                    col_list1 = np.absolute(np.fft.fft(col_list1))
                    x.append(np.divide(col_list1[1:], col_list1[0]))

                    start_time+=step
                    end_time+=step

                if fire_flag:
                    pc = pca.fit_transform(np.array(x))
                    data['fire']['x'].append(pc[:,0].tolist())
                    data['fire']['y'].append(pc[:,1].tolist())
                    data['fire']['z'].append(pc[:,2].tolist())
                else:
                    pc = pca.fit_transform(np.array(x))
                    data['nofire']['x'].append(pc[:,0].tolist())
                    data['nofire']['y'].append(pc[:,1].tolist())
                    data['nofire']['z'].append(pc[:,2].tolist())


    return data



def scatter3d(df):

    # data = get_scatter(df)
    for dist_start in range(1, 50):
        data = makePCA(df, dist_start)


        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(data['nofire']['x'], data['nofire']['y'], data['nofire']['z'], c='g', marker='o')
        ax.scatter(data['fire']['x'], data['fire']['y'], data['fire']['z'], c='r', marker='o')

        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')
        ax.set_xlim([-4, 4])
        ax.set_ylim([-4, 4])
        ax.set_zlim([-4, 4])

        plt.show()

def scatter2d(data):


    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(data['nonfire']['slope'], data['nonfire']['variance'], c='g', alpha=.5, s=2, marker='o')
    ax.scatter(data['fire']['slope'], data['fire']['variance'], c='r', s=2, marker='o')

    ax.set_title(sys.argv[1], fontsize=18)
    ax.set_xlabel('Slope of FFT')
    ax.set_ylabel('Variance of Signal (percent difference)')
    # ax.set_zlabel('Z Label')
    # ax.set_xlim([-0.0025, 0.0001])
    # ax.set_ylim([-4, 4])
    # ax.set_zlim([-4, 4])

    plt.show()


#generates scatterplot or line plot of a single distance from the sensor, over all timesteps
def line_plot_distance(x, y, dist):
    # fig = plt.figure()
    # ax = fig.add_subplot(1,1,1,axisbg='1.0')
    # ax.scatter(x, y, alpha=0.7, edgecolors = 'none', s=30)
    plt.figure(figsize=(14,5))
    plt.plot(x, y, c='b', linewidth=7.0)
    plt.ylim(bottom=0) #, top=250000000000)
    plt.xlim(left=0)
    distance = (dist * 4.4)/100
    # plt.title("Distance (m): " + str(round(distance, 2)) + " Bin: " + str(dist), fontsize=18)
    plt.ylabel('Reflection Power (dB)', fontsize=28)
    plt.xlabel('Timesteps (10/second)', fontsize=28)
    plt.xticks(fontsize=28)
    plt.yticks(fontsize=28)
    plt.show()

#generates line plot for each timesetp, like the GUI on the mmwave site
def line_plot(x, y, time):
    plt.plot(x, y)
    plt.ylim(bottom=0, top=4000000000000)
    plt.xlim(left=0)
    plt.title("Timestep: " + str(time), fontsize=18)
    plt.ylabel('Reflection Power', fontsize=15)
    plt.xlabel('Distance', fontsize=15)
    plt.show()

def graphAll(all_same_plot, time, start):
    for i, a in enumerate(all_same_plot):
        plt.plot(time, a, label=i+start+1)
    plt.ylim(bottom=0) #, top=250000000000)
    plt.xlim(left=0)
    plt.legend(loc=1, ncol=2, frameon=True)
    plt.ylabel('Reflection Power', fontsize=18)
    plt.xlabel('Timesteps (10/sec)', fontsize=18)
    plt.show()

#loads the data into pandas dataframe, deletes the last 5 lines to avoid anomalies
def loadData(fname=None, dirname=None):
    if fname is None:
        fname = sys.argv[len(sys.argv) - 1]
        path = fname + '/'+ fname + '.csv'
    else:
        path = dirname + fname + '/'+ fname + '.csv'
    data = pd.read_csv(path, index_col=False)
    return data.iloc[:-5]

def fun(x, t, y):
    return x[0] * np.exp(-x[1] * t) * np.sin(x[2] * t) - y

def fft(df):

    target_distance = 173
    # 45, 89, 135, 181 fires

    for i, col in enumerate(df.columns):
        if i == target_distance:
            col_list = df[col].tolist()
            # col_list = np.log10(col_list[800:1312])
            col_list = np.array(col_list[800:1100])

            col_list = np.absolute(np.fft.fft(col_list))
            col_list1 = np.divide(col_list[1:], col_list[0])
            col_list = col_list1
            time = list(range(len(col_list)))

            # col_list = np.multiply(np.log10(col_list), 10)

            plt.plot(time, col_list)
            plt.title("Distance: " + str(i) + " - " + str(sys.argv[len(sys.argv) - 1]), fontsize=18)
            plt.axhline(y=0, color='k')
            plt.ylim(bottom=0, top=0.04)
            plt.show()




# This function finds the varience and slope factor of each sample, and plots it on a scatterplot
def make2Dscatter(df):
    dist_start = 1
    # min_fire_bin = 66
    # dist_stop = min_fire_bin + 40
    min_fire_bin = 180
    dist_stop = 180

    all_avg = 0
    all_max = 0
    all_min = 10
    all_count = 0

    max_variance = 0
    min_variance = 10

    data = {'fire':{'variance':[], 'slope':[]},
            'nonfire':{'variance':[], 'slope':[]}}


    for j in range(dist_start, dist_stop):
        all_count += 1

        target_distance = j
        # 45, 89, 135, 181 fires

        start_time = 0
        end_time = start_time + 300
        step = 10

        fire_start = 600

        total_max = 0
        total_min = 10
        total_total = 0
        total_count = 0

        for i, col in enumerate(df.columns):
            if i == target_distance:
                print("Distance: ", i)
                col_list = df[col].tolist()
                fire_flag = False

                while end_time < len(col_list):
                    if target_distance < min_fire_bin:
                        fire_flag = False
                    else:
                        if end_time < fire_start:
                            fire_flag = False
                        elif start_time < 750:
                            start_time+=step
                            end_time+=step
                            continue
                        else:
                            fire_flag = True


                    total_count += 1
                    col_list1 = np.array(col_list[start_time:end_time])
                    fft = np.absolute(np.fft.fft(col_list1))
                    fft = np.divide(fft[1:], fft[0])

                    fft = fft[2:150]
                    x_axis = list(range(len(col_list)))
                    x_axis_fft = list(range(len(fft)))


                    time = np.linspace(0, len(fft), len(fft))
                    fft_p = np.poly1d(np.polyfit(x_axis_fft, fft, 1))
                    slope = fft_p[1]

                    time = np.linspace(0, len(col_list1), len(col_list1))
                    col_list1_p = np.poly1d(np.polyfit(time, col_list1, 8))
                    col_list1_mean = col_list1_p(time)

                    variance_total = 0
                    for c_count, c in enumerate(col_list1):
                        variance_total+= (abs(c - col_list1_mean[c_count]) / c)

                    variance = variance_total/(col_list1.shape[0])

                    if fire_flag:
                        data['fire']['slope'].append(slope)
                        data['fire']['variance'].append(variance)
                    else:
                        data['nonfire']['slope'].append(slope)
                        data['nonfire']['variance'].append(variance)

                    start_time+=step
                    end_time+=step



    scatter2d(data)













from scipy.signal import argrelextrema

def fft_calculation(df):

    dist_start = 68
    dist_stop = dist_start + 40

    all_avg = 0
    all_max = 0
    all_min = 10
    all_count = 0

    max_variance = 0
    min_variance = 10

    min_R = 10
    max_R = 0
    min_sigma = 10
    max_sigma = 0

    data = {'fire':{'variance':[], 'slope':[]},
            'nonfire':{'variance':[], 'slope':[]}}


    for j in range(dist_start, dist_stop):
        all_count += 1

        target_distance = j
        # 45, 89, 135, 181 fires

        start_time = 750
        end_time = start_time + 300
        # start_time = 750
        # end_time = 1050
        step = 10

        fire_start = 600

        total_max = 0
        total_min = 10
        total_total = 0
        total_count = 0

        for i, col in enumerate(df.columns):
            if i == target_distance:
                col_list = df[col].tolist()
                fire_flag = False
                while end_time < len(col_list):
                    if end_time > fire_start and start_time < fire_start:
                        if j == dist_stop-1:
                            fire_flag = True
                            # print("broke out at ", total_count)
                        break

                    total_count += 1
                    # col_list = np.log10(col_list[800:1312])
                    col_list1 = np.array(col_list[start_time:end_time])
                    np_col_list = col_list1
                    col_list1 = np.absolute(np.fft.fft(col_list1))
                    col_list1 = np.divide(col_list1[1:], col_list1[0])

                    fft = col_list1[1:150]
                    x_axis = list(range(len(col_list)))
                    x_axis_fft = list(range(len(fft)))

                    time = np.linspace(0, len(fft), len(fft))
                    p = np.poly1d(np.polyfit(x_axis_fft, fft, 8))
                    mean = p(time)

                    # FFT calculations
                    # time = np.linspace(0, len(fft), len(fft))
                    fft_p = np.poly1d(np.polyfit(x_axis_fft, fft, 1))
                    slope = fft_p[1]
                    fft_linear_fit = fft_p(time)



                    # sigma^2
                    fft_mean = np.mean(fft)
                    sum = 0
                    for c in np_col_list:
                        sum+=(((c - np.mean(np_col_list)) / c)**2)
                    sigma_squared = sum / np_col_list.shape[0]

                    if sigma_squared < min_sigma:
                        min_sigma = sigma_squared
                    if sigma_squared > max_sigma:
                        max_sigma = sigma_squared

                    # R^2
                    s_tot = 0
                    s_res = 0
                    for fft_count, f in enumerate(fft):
                        s_tot += ((f - fft_mean)**2)
                        s_res += ((f - fft_linear_fit[fft_count])**2)
                    R_squared = (1 - (s_res / s_tot))

                    if R_squared < min_R:
                        min_R = R_squared
                    if R_squared > max_R:
                        max_R = R_squared

                    # print("Distance: ", i, "  R: ", R_squared, " Sig: ", sigma_squared)

                    if fire_flag:
                        data['fire']['slope'].append(R_squared)
                        data['fire']['variance'].append(sigma_squared)
                    else:
                        data['nonfire']['slope'].append(R_squared)
                        data['nonfire']['variance'].append(sigma_squared)


                    variance_total = 0
                    for fft_count, f in enumerate(fft):
                        variance_total+= abs(f - mean[fft_count])

                    variance = variance_total/(fft.shape[0])


                    if variance < min_variance:
                        min_variance = variance
                    if variance > max_variance:
                        max_variance = variance



                    max_idx = argrelextrema(col_list1, np.greater)
                    min_idx = argrelextrema(col_list1, np.less)
                    maxs = col_list1[argrelextrema(col_list1, np.greater)[0]]
                    mins = col_list1[argrelextrema(col_list1, np.less)[0]]

                    try:
                        if max_idx[0][0] < min_idx[0][0]:
                            a = maxs
                            b = mins
                        else:
                            a = mins
                            b = maxs

                        c = np.empty((a.size + b.size,), dtype=a.dtype)
                        c[0::2] = a
                        c[1::2] = b

                    except:
                        if max_idx[0].size == 0:
                            c = mins
                        elif min_idx[0].size == 0:
                            c = maxs
                        else:
                            c = np.array([])


                    total_diff = 0
                    count = 0
                    for key, val in enumerate(c):
                        try:
                            total_diff+=abs(val - c[key+1])
                            count+=1
                        except:
                            pass

                    try:
                        avg = total_diff/count
                    except:
                        avg = 0
                    # print(i, " avg = ", avg)
                    total_total+=avg

                    if avg > total_max:
                        total_max = avg
                    if avg < total_min:
                        total_min = avg


                    start_time+=step
                    end_time+=step

        # print(j, " total average: ", total_total/total_count, "         max: ", total_max, "        min: ", total_min)
        all_avg+=(total_total/total_count)

        if total_max > all_max:
            all_max = total_max
        if total_min < all_min:
            all_min = total_min

    # print()
    # print("Average: ", all_avg/all_count, "   max: ", all_max, "    min: ", all_min)
    # print("max var: ", max_variance)
    # print("min var: ", min_variance)
    scatter2d(data)
    # print("R - min:     ", min_R, "        max: ", max_R)
    # print("Sigma - min: ", min_sigma, "    max: ", max_sigma)


def getSlope(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    slope = fft_p[1]
    return slope



SIGNAL_VARIANCE_MAX_THRESHOLD = 0.15 # 0.25 for rfire_v-1.h5
SIGNAL_VARIANCE_MIN_THRESHOLD = 0.02 #0.09
SIGNAL_MVAR_MAX_THRESHOLD = 1.6 # MVAR means this is the threshold for the max_variance variable
SIGNAL_MVAR_MIN_THRESHOLD = 0.05

FFT_MAX_THRESHOLD = 0.0065
FFT_MIN_THRESHOLD = 0.0025
FFT_MAX_VARIANCE = 0.007
FFT_MIN_VARIANCE = 0.0015
FFT_SLOPE_MAX_THRESHOLD = 0
FFT_SLOPE_MIN_THRESHOLD = -0.001
min_slope = 0
max_slope = -10

R_SQUARED_MAX_THRESHOLD = 0.4 # MAYBE 0.25
R_SQUARED_MIN_THRESHOLD = 0.001
SIG_SQUARED_MAX_THRESHOLD = 1
SIG_SQUARED_MIN_THRESHOLD = 0.001

MIN_REF_THRESH = 1000000
PERIODICY_MIN_THRESH = 0


def same_as_samplefactory(df):
    target_distance = 6

    start_time = 0
    end_time = start_time + 300
    # start_time = 750
    # end_time = 1050
    step = 10

    fire_start = 6000000

    total_count = 0
    for i, col in enumerate(df.columns):
        if i == target_distance:
            col_list1 = df[col].tolist()
            while end_time < len(col_list1):


                variance = getVariance(col_list1[start_time:end_time])
                fft_variance = getVariance(col_list1[start_time:end_time], fft_bool=True)
                fft_slope = getSlope(col_list1[start_time:end_time])

                if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE:
                    print(int(start_time/10), " should be included")

                start_time+=step
                end_time+=step

                if total_count == 200:
                    exit()
                total_count+=1


def getVariance(lst, fft_bool=False):

    lst = np.array(lst)

    if fft_bool:
        degree = 1
        lst_fft = np.absolute(np.fft.fft(lst))
        fft = np.divide(lst_fft[1:], lst_fft[0])
        lst = fft[5:150]
        x_axis_fft = list(range(len(lst)))
    else:
        degree = 4

# this gets the signal variance (percentage of signal strength instead of total number)
# fires are around 0.10 < fire < 0.17
    time = np.linspace(0, len(lst), len(lst))
    p = np.poly1d(np.polyfit(time, lst, degree))
    mean = p(time)

    variance_total = 0
    cur_max = 0
    cur_max_not_perc = 0
    for count, f in enumerate(lst):
        if fft_bool:
            variance_total+= abs(f - mean[count])
        else:
            cur = (abs(f - mean[count]) / f)
            variance_total+= cur
            if cur > cur_max:
                cur_max = cur
            cur_not_perc = abs(f - mean[count])
            if cur_not_perc > cur_max_not_perc:
                cur_max_not_perc = cur_not_perc

    return variance_total/(lst.shape[0]), cur_max, mean, lst

def getPeriodicy(lst, fit):
    x = np.linspace(0, len(lst), len(lst))

    if lst[0] > fit[0]:
        prev_max = "lst"
        cur_max = "lst"
    else:
        prev_max = "fit"
        cur_max = "fit"

    crosses = 0
    for i, val in enumerate(x):
        prev_max = cur_max
        if lst[i] > fit[i]:
            cur_max = "lst"
        if fit[i] > lst[i]:
            cur_max = "fit"
        if prev_max != cur_max:
            crosses+=1

    # plt.plot(list(range(len(lst))), lst)
    # plt.plot(list(range(len(fit))), fit)
    # plt.show()
    return crosses


def getSlope(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    slope = fft_p[1]
    return slope

def getRandSigma(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    fft_linear_fit = fft_p(time)

    # sigma^2
    sum = 0
    for c in lst:
        sum+=(((c - np.mean(lst)) / c)**2)
    sigma_squared = sum / lst.shape[0]

    s_tot = 0
    s_res = 0
    for fft_count, f in enumerate(fft):
        s_tot += ((f - np.mean(fft))**2)
        s_res += ((f - fft_linear_fit[fft_count])**2)
    R_squared = (1 - (s_res / s_tot))

    return R_squared, sigma_squared


def getFFTAvg(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[2:150]

    max_idx = argrelextrema(fft, np.greater)
    min_idx = argrelextrema(fft, np.less)
    maxs = fft[argrelextrema(fft, np.greater)[0]]
    mins = fft[argrelextrema(fft, np.less)[0]]

    try:
        if max_idx[0][0] < min_idx[0][0]:
            a = maxs
            b = mins
        else:
            a = mins
            b = maxs

        c = np.empty((a.size + b.size,), dtype=a.dtype)
        c[0::2] = a
        c[1::2] = b

    except:
        if max_idx[0].size == 0:
            c = mins
        elif min_idx[0].size == 0:
            c = maxs
        else:
            c = np.array([])

    total_diff = 0
    count = 0
    for key, val in enumerate(c):
        try:
            total_diff+=abs(val - c[key+1])
            count+=1
        except:
            pass

    try:
        avg = total_diff/count
    except:
        avg = 0
    return avg




def show_sig_and_fft(df):

    target_distance = 23
    end_dist = target_distance+40
    # 45, 89, 135, 181 fires



    while target_distance < end_dist:
        start_time = 600
        end_time = start_time + 300
        # start_time = 750
        # end_time = 1050
        step = 10

        fire_start = 6000000

        total_max = 0
        total_min = 10
        total_total = 0
        total_count = 0
        for i, col in enumerate(df.columns):
            max_variance = 0
            min_variance = 10
            if i == (target_distance):
                print("Target Distance: ", i)
                col_list1 = df[col].tolist()
                # col_list_first = col_list1[0:624]
                # col_list_second = col_list1[644:]
                # col_list1 = col_list_first + col_list_secondvars = []
                vars = []
                fft_vars = []
                max_sig_var_arr = []
                start_time_base = 0
                end_time_base = 300
                step = 10
                while end_time_base < 600:
                    variance, max_variance,_,_ = getVariance(col_list1[start_time_base:end_time_base])
                    fft_variance,_,_,_= getVariance(col_list1[start_time_base:end_time_base], fft_bool=True)
                    vars.append(variance)
                    fft_vars.append(fft_variance)
                    max_sig_var_arr.append(max_variance)
                    start_time_base+=step
                    end_time_base+=step
                baseline_variance = max(vars)
                baseline_max_variance = max(max_sig_var_arr)
                baseline_fft_variance = max(fft_vars)
                while end_time < len(col_list1):
                    if end_time > fire_start and start_time < fire_start:
                        print("broke out at ", total_count)
                        break


                    # NOTE: for fires, variance is low, and slope is negative and "bigger" than zero

                    total_count += 1
                    col_list = col_list1[start_time:end_time]

                    variance, cur_max, col_list_fit, col_list_returned = getVariance(col_list)
                    variance = variance / baseline_variance
                    # print(col_list_fit)
                    # plt.plot(list(range(len(col_list_fit))), col_list_fit)
                    # plt.plot(list(range(len(col_list_fit))), col_list_returned)
                    # plt.show()
                    # exit()
                    fft_variance,cur_max_fft, fft_fit, fft_returned = getVariance(col_list, fft_bool=True)

                    fft_variance = fft_variance / baseline_fft_variance

                    fft_slope = getSlope(col_list)
                    R_squared, sigma_squared = getRandSigma(col_list)
                    periodicy = getPeriodicy(col_list_returned, col_list_fit)

                    x_axis = list(range(len(col_list_returned)))
                    time = np.linspace(0, len(fft_returned), len(fft_returned))


                    # if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE and np.amax(col_list) > MIN_REF_THRESH and periodicy > PERIODICY_MIN_THRESH:
                    print(start_time, " - ", end_time, " signal variance: ", variance, " fft slope: ", fft_slope, " fft var: ", fft_variance, " signal max_var: ", cur_max, " periodicy: ", periodicy)
                    # else:
                    #     if variance > SIGNAL_VARIANCE_MAX_THRESHOLD or variance < SIGNAL_VARIANCE_MIN_THRESHOLD:
                    #         print(start_time, " - ", end_time, " sig variance: ", variance)
                    #     if fft_slope > FFT_SLOPE_MAX_THRESHOLD:
                    #         print(start_time, " - ", end_time, " FFT slope: ", fft_slope)
                    #     if fft_variance > FFT_MAX_VARIANCE or fft_variance < FFT_MIN_VARIANCE:
                    #         print(start_time, " - ", end_time, " FFT variance: ", fft_variance)
                    #     if np.amax(col_list) < MIN_REF_THRESH:
                    #         print(start_time, " - ", end_time, " max reflection: ", np.amax(col_list))
                    #     if periodicy < PERIODICY_MIN_THRESH:
                    #         print(start_time, " - ", end_time, " periodicy: ", periodicy)
                    # print(start_time, " - ", end_time, " fft Variance: ", fft_variance)
                    # print(start_time, " - ", end_time, "    R: ", str(round(R_squared, 4)), "         Sigma: ", str(round(sigma_squared, 8)))
                    # print(start_time, " - ", end_time, " signal variance: ", variance, " signal max_var: ", cur_max, " fft slope: ", fft_slope, " fft variance: ", fft_variance)


                    # plt.subplot(211)
                    # plt.plot(x_axis, col_list_returned)
                    # plt.plot(x_axis, col_list_fit)
                    # plt.title("Distance: " + str(i) + " - Reflection time: " + str(start_time) + " - " + str(end_time), fontsize=10)
                    # plt.axhline(y=0, color='k')
                    # # plt.show()
                    #
                    # plt.subplot(212)
                    # plt.plot(time, fft_returned)
                    # plt.plot(time, fft_fit)
                    # plt.title("Distance: " + str(i) + " - FFT", fontsize=10)
                    # # plt.xlabel("R^2:       " + str(round(R_squared, 4)) + "         Sigma^2:       " + str(round(sigma_squared, 8)), fontsize=10)
                    # # plt.xlabel("Average: " + str(round(avg, 4)) + "  Sig-Variance: " + str(round(variance, 4)) + "   FFT-Slope: " + str(round(fft_slope, 8)), fontsize=10)
                    # plt.xlabel("  Sig-Variance: " + str(round(variance, 4)) + "   Max-varience: " + str(round(cur_max, 8)), fontsize=10)
                    # plt.axhline(y=0, color='k')
                    # plt.ylim(bottom=0, top=0.05)
                    #
                    # plt.show()
                    # exit()
                    if variance < min_variance:
                        min_variance = variance
                    if variance > max_variance:
                        max_variance = variance

                    start_time+=step
                    end_time+=step
                    # if total_count == 60:
                    #     exit()
        target_distance+=1


from scipy.io.wavfile import write
from scipy.io import wavfile

def spectrogram(df):
    target_distance = 45

    for i, col in enumerate(df.columns):
        if i == target_distance:
            col_list = np.array(df[col].tolist())
            # col_list = np.log10(col_list[800:1312])
            # col_list = col_list[1000:2024]
            # col_list1 = col_list[:600]
            # col_list2 = col_list[750:]
            # col_list = col_list1 + col_list2
            x_axis = list(range(len(col_list)))

            samplingFrequency = len(col_list)

            # write('test.wav', samplingFrequency, col_list)
            # samplingFrequency, signalData = wavfile.read('test.wav')

            plt.subplot(211)
            plt.plot(x_axis, col_list)
            plt.title("Distance: " + str(i) + " - " + str(sys.argv[len(sys.argv) - 1]), fontsize=18)
            plt.axhline(y=0, color='k')
            # plt.show()

            plt.subplot(212)
            powerSpectrum, freqenciesFound, time, imageAxis = plt.specgram(col_list, Fs=samplingFrequency)
            plt.xlabel('Time')
            plt.ylabel('Frequency')
            plt.show()


#takes the columns of the CSVs. So there are 256 distances in a single direction
def parseByDistance(df):
    all_same_plot = []
    lower_bound = 40
    upper_bound = lower_bound + 40
    for i, col in enumerate(df.columns):
        if i < upper_bound and i > lower_bound:
            col_list = df[col].tolist()
            col_list1 = col_list[:600]
            col_list2 = col_list[750:]
            col_list = col_list1 + col_list2
            time = list(range(len(col_list)))

            # new_col_list = []
            # idx = 20
            # for j, val in enumerate(col_list):
            #     new_col_list.append(average(col_list[idx-20:idx]))
            #     idx+=20
            #     if idx > len(col_list):
            #         break
            # col_list = new_col_list
            # print(str(min(col_list)))
            # line_plot_distance(time, col_list, i)
            all_same_plot.append(col_list)
            # exit()
    graphAll(all_same_plot, time, lower_bound)


def average(lst):
    return sum(lst) / len(lst)

def viewTopPercentage(df):

    for i, col in enumerate(df.columns):
        col_list = df[col].tolist()
        baseline = average(col_list[:300])
        if baseline < 4000000:
            continue
        for j in col_list[300:]:
            diff = abs(baseline - j)
            if (diff / baseline) > 4:
                # print((diff / baseline))
                print(i)
                time = list(range(len(col_list)))
                line_plot_distance(time, col_list, i)
                break



def viewTopDistances(df):
    maximums = []
    for i, col in enumerate(df.columns):
        col_list = df[col].tolist()
        if i > 5:
            maximums.append(max(col_list))

    top = sorted(maximums)[-15:]
    for i, col in enumerate(df.columns):
        col_list = df[col].tolist()
        if max(col_list) in top:
            time = list(range(len(col_list)))
            line_plot_distance(time, col_list, i)


#Plots each row of the CSV. This is like the GUI in the mmWave Demo, showing all distances for time t
def parseByTime(df):
    for idx, rows in df.iterrows():
        # if idx%10 == 0:
        if idx == 1:
            power_arr = rows.tolist()
            time = list(range(len(power_arr)))
            line_plot(time, power_arr, idx)
            exit()

try:
    df = loadData()
except:
    path = (sys.argv[len(sys.argv) - 1]) + '/'
    flag = False
    for idx, f in enumerate(sorted(listdir(path))):
        # print(f)
        if f[0] is not '_':
            if f[0] is not '.':
                returned_df = loadData(f, path)
                if flag == False:
                    flag = True
                    df = returned_df
                    continue

                df = pd.concat([df, returned_df])

    df = df.reset_index(drop=True)

# spectrogram(df)
# fft(df)
# fft_calculation(df)
show_sig_and_fft(df)
# same_as_samplefactory(df)
# parseByDistance(df)
# parseByTime(df)
# viewTopDistances(df)
# viewTopPercentage(df)
# threeDimPlot(df)
# scatter3d(df)
# make2Dscatter(df)
